package tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import model.Operaciones;

public class OperationTest {
	
	public Operaciones op;
	
	@Before
	public void setup()
	{
		op = new Operaciones(2,3);
	}
	
	@Test
	public void sumarTest()
	{
		assertEquals(5, op.sumar());
	}

}
